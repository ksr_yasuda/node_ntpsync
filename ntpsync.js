#!/usr/bin/env node

const childProcess		= require("child_process");

const ntpClient			= require("ntp-client");

//==============================================================================

function errorHandler(err){
	if(err == null){
		return;
	}

	console.error(err);
	process.exit(1);
}

//==============================================================================

ntpClient.getNetworkTime("ntp.jst.mfeed.ad.jp", 123, (err, date) => {
	if(err != null){
		errorHandler(err);
		return;
	}

	//console.log(date);

	//Adjust extra milliseconds.
	//Set the tiem after the following next second.
	let extraMsec		= 1000 + (1000 - date.getMilliseconds());
	date		= new Date(date.getTime() + extraMsec);
	//console.log(date);

	let dateStr		= `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
	//console.log(dateStr);

	setTimeout(() => {
		if(process.argv.slice(2).indexOf("--dry-run") >= 0){
			//console.log("--dry-run");
			console.info(dateStr);
		} else {
			let dateProc		= childProcess.spawn("date", [dateStr], {
									stdio:		/**/"ignore"/*/"inherit"/**/
								}).on("error", errorHandler);
		}
	}, extraMsec);
});
